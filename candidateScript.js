const puppeteer = require('puppeteer')
const select = require('puppeteer-select')
;(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    ignoreHTTPSErrors: true,
    args: [`--window-size=1920,1080`],
    defaultViewport: {
      width: 1920,
      height: 1080,
    },
  })
  const page = await browser.newPage()

  const navigationPromise = page.waitForNavigation()


  await page.goto('https://fr.indeed.com/jobs?q=developpeur%20react&l=')

  await page.waitForTimeout(120000)

  const result = await page.evaluate(() =>
    [...document.querySelectorAll('#mosaic-provider-jobcards > a')].map(
      (link) => {
        console.log(link)
        if (link.href.includes('https://fr.indeed.com/company/')) {
          return link.href
        } else {
          return null
        }
      },
    ),
  )

  await navigationPromise

  const filterLink = await result.filter((link) => {
    return link != null
  })

  // const element = await page.evaluate(() => {
  //   return document.getElementById('#indeedApplyButton')
  // })

  const form = await page.$('button#indeedApplyButton')
  // const form2 = await page.$('button#ia-IndeedResumeSelect-headerButton')

  await navigationPromise
  ;(await filterLink) &&
    filterLink.map((link) => {
      page.goto(link)
      page.waitForTimeout(2000)
      page.$eval('button#indeedApplyButton', (form) => form.click())
      // page.waitForTimeout(2000)
      // page.click('[id="indeedApplyButton"]')
      page.waitForTimeout(5000)
    })

  await page.waitForTimeout(2000)

  const [button] = await page.$x("//button[contains(., 'Postuler')]")
  if (button) {
    await button.click()
  }

  await page.waitForSelector('input[type="email"]')
  await page.waitForTimeout(2000)
  await page.click('input[type="email"]')

  await navigationPromise



  console.log(filterLink)
})()
